import time

from typing import List
from collections import defaultdict
from urllib import parse

import requests

from statuskit.obj import Trigger

from ._logger import logger
from .excepts import (ErrLoginZabbix, ErrConnectionErrorZabbix)


class Zabbix:

    def __init__(self, url: str, user: str, passwd: str, debug: bool = True) -> None:
        self.url = url
        self.user = user
        self.passwd = passwd
        self.debug = debug
        self.token = None
        self.triggers = defaultdict(time.time)
        self.ini_timestamp = time.time()

        self.__login()

    def __login(self) -> None:
        data_login = {
            "jsonrpc": "2.0",
            "method": "user.login",
            "params": {
                "user": self.user,
                "password": self.passwd
            },
            "id": 1,
            "auth": None}

        try:
            req = requests.post(
                self.url, json=data_login)
        except requests.exceptions.ConnectionError as err:
            url_parse = parse.urlsplit(self.url)
            raise ErrConnectionErrorZabbix(err, host=url_parse.netloc, port=url_parse.scheme)

        result: dict = req.json()
        if result.get('result') is None:
            raise ErrLoginZabbix(**result.get('error'))

        logger.debug("login successfully with user '%s' in url '%s'." % (self.user, self.url))

        self.token = result.get('result')

    def host_by_name(self, name: str) -> dict:
        """
        Reference: https://www.zabbix.com/documentation/4.0/manual/api/reference/host/get#retrieving_data_by_name

        {   'auto_compress': '1',
            'available': '0',
            'description': '',
            'disable_until': '0',
            'error': '',
            'errors_from': '0',
            'flags': '0',
            'host': 'FL57 - GUARABIRA 2 - TELY',
            'hostid': '10461',
            'inventory_mode': '1',
            'ipmi_authtype': '-1',
            'ipmi_available': '0',
            'ipmi_disable_until': '0',
            'ipmi_error': '',
            'ipmi_errors_from': '0',
            'ipmi_password': '',
            'ipmi_privilege': '2',
            'ipmi_username': '',
            'jmx_available': '0',
            'jmx_disable_until': '0',
            'jmx_error': '',
            'jmx_errors_from': '0',
            'lastaccess': '0',
            'maintenance_from': '0',
            'maintenance_status': '0',
            'maintenance_type': '0',
            'maintenanceid': '0',
            'name': 'FL57 - GUARABIRA 2 - TELY',
            'proxy_address': '',
            'proxy_hostid': '10933',
            'snmp_available': '0',
            'snmp_disable_until': '0',
            'snmp_error': '',
            'snmp_errors_from': '0',
            'status': '0',
            'templateid': '0',
            'tls_accept': '1',
            'tls_connect': '1',
            'tls_issuer': '',
            'tls_psk': '',
            'tls_psk_identity': '',
            'tls_subject': ''}
        """

        data_post = {
            "jsonrpc": "2.0",
            "method": "host.get",
            "params": {
                "filter": {
                    "host": [name]
                }
            },
            "auth": self.token,
            "id": 1
        }

        try:
            req = requests.get(self.url, json=data_post)
        except requests.exceptions.ConnectionError as err:
            url_parse = parse.urlsplit(self.url)
            raise ErrConnectionErrorZabbix(err, host=url_parse.netloc, port=url_parse.scheme)

        return req.json()

    def events_by_triggerids(self, triggerids: List[Trigger]) -> dict:
        # Reference: https://www.zabbix.com/documentation/4.0/manual/api/reference/event/get#retrieving_trigger_events

        for triggerid in triggerids:
            if triggerid not in self.triggers:
                self.triggers[triggerid.triggerid] = self.ini_timestamp

        data_post = {
            "jsonrpc": "2.0",
            "method": "event.get",
            "params": {
                "output": "extend",
                "time_from": self.ini_timestamp,
                "objectids": [t.triggerid for t in triggerids],
                "sortfield": ["clock", "eventid"],
                "sortorder": "DESC"
            },
            "auth": self.token,
            "id": 1
        }

        try:
            req = requests.get(self.url, json=data_post)
        except requests.exceptions.ConnectionError as err:
            url_parse = parse.urlsplit(self.url)
            raise ErrConnectionErrorZabbix(err, host=url_parse.netloc, port=url_parse.scheme)

        return req.json()

    def event_by_triggerid(self, triggerid: int) -> dict:
        # Reference: https://www.zabbix.com/documentation/4.0/manual/api/reference/event/get#retrieving_trigger_events

        if triggerid not in self.triggers:
            self.triggers[triggerid] = self.ini_timestamp

        from_timestamp = str(int(self.triggers[triggerid]))

        data_post = {
            "jsonrpc": "2.0",
            "method": "event.get",
            "params": {
                "output": "extend",
                "time_from": from_timestamp,
                "objectids": triggerid,
                "sortfield": ["clock", "eventid"],
                "sortorder": "DESC"
            },
            "auth": self.token,
            "id": 1
        }

        try:
            req = requests.get(self.url, json=data_post)
        except requests.exceptions.ConnectionError as err:
            url_parse = parse.urlsplit(self.url)
            raise ErrConnectionErrorZabbix(err, host=url_parse.netloc, port=url_parse.scheme)

        return req.json()

    def trigger_by_hostid(self, hostid: str or list) -> dict:
        # Reference: https://www.zabbix.com/documentation/4.0/manual/api/reference/trigger/get#retrieving_data_by_trigger_id

        hostid = str(hostid) if isinstance(hostid, int) else hostid

        data_post = {
            "jsonrpc": "2.0",
            "method": "trigger.get",
            "params": {
                "output": [
                    "triggerid",
                    "description",
                    "priority"
                ],
                "hostids": hostid
            },
            "auth": self.token,
            "id": 1
        }

        try:
            req = requests.get(self.url, json=data_post)
        except requests.exceptions.ConnectionError as err:
            url_parse = parse.urlsplit(self.url)
            raise ErrConnectionErrorZabbix(err, host=url_parse.netloc, port=url_parse.scheme)

        return req.json()

    def hosts_by_group(self, group_id: int or str):
        data_post = {
            "jsonrpc": "2.0",
            "method": "host.get",
            "params": {
                "groupids": "%s" % group_id
            },
            "auth": self.token,
            "id": 1
        }

        try:
            req = requests.get(self.url, json=data_post)
        except requests.exceptions.ConnectionError as err:
            url_parse = parse.urlsplit(self.url)
            raise ErrConnectionErrorZabbix(err, host=url_parse.netloc, port=url_parse.scheme)

        return req.json()

    def group_by_id(self, group_id: str or int):
        data_post = {
            "jsonrpc": "2.0",
            "method": "hostgroup.get",
            "params": {
                "output": "extend",
                "groupids": "%s" % group_id
            },
            "auth": self.token,
            "id": 1
        }

        return Zabbix.exec_request(self.url, data_post)

    def groups(self) -> dict:
        """
        { 'id': 1,
          'jsonrpc': '2.0',
          'result': [ { 'flags': '0',
                        'groupid': '1',
                        'internal': '0',
                        'name': 'Templates'},
                    ]}
        """
        data_post = {
            "jsonrpc": "2.0",
            "method": "hostgroup.get",
            "params": {
                "output": "extend",
                "filter": {}
            },
            "auth": self.token,
            "id": 1
        }

        return Zabbix.exec_request(self.url, data_post)

    @staticmethod
    def exec_request(url, payload: dict):
        try:
            req = requests.get(url, json=payload)
        except requests.exceptions.ConnectionError as err:
            url_parse = parse.urlsplit(url)
            raise ErrConnectionErrorZabbix(err, host=url_parse.netloc, port=url_parse.scheme)

        return req.json()
