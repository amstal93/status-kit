

class ErrLoginZabbix(Exception):

    def __init__(self, message, code, data):
        super().__init__(message)

        self.message = message
        self.code = code
        self.data = data

    def __str__(self):
        return "[%d][%s] %s" % (self.code, self.message.replace('.', ''), self.data)


class ErrConnectionErrorZabbix(Exception):

    def __init__(self, message, host, port):
        super().__init__(message)

        self.message = message
        self.host = host
        self.port = port
