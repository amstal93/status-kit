DOCKER_COMPOSE ?= docker-compose

all: compose

# Down and Up all containers on depends to the project run
COMPOSE_FILE_NAME ?= docker-compose.yml
COMPOSE_DEV_FILE_NAME ?= docker-compose.yml

compose: compose-down compose-up
docker: docker-build

compose-dev:
	$(DOCKER_COMPOSE) -f $(COMPOSE_DEV_FILE_NAME) up -d --build --remove-orphans --force-recreate
# Down all containers on depends to the project run
compose-down:
	$(DOCKER_COMPOSE) -f $(COMPOSE_FILE_NAME) down -v
# Up all containers on depends to the project run
compose-up:
	$(DOCKER_COMPOSE) -f $(COMPOSE_FILE_NAME) up -d --build --force-recreate --remove-orphans

docker-build:
	docker build -t statuskit:latest .