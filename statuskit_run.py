import os

import click

from statuskit.core import StatusKit


@click.command()
@click.option('--zabbix-url', help='Zabbix api url')
@click.option('--zabbix-user', help='user in Zabbix')
@click.option(
    '--zabbix-password', help='user in Zabbix', prompt=True, hide_input=True,
    default=lambda: os.environ.get("ZABBIX_USER", ""))
@click.option('--statuskit-url', help='Statuskit api url', default='http://localhost:8000/api/components/')
def run(zabbix_url, zabbix_user, zabbix_password, statuskit_url):
    stk = StatusKit(
        zabbix_user=zabbix_user,
        zabbix_password=zabbix_password,
        zabbix_url=zabbix_url,
        statuskit_url=statuskit_url
    )
    stk.run()


if __name__ == '__main__':
    run()
